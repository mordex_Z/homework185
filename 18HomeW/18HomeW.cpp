﻿#include <iostream>

template<typename T>
class Stack
{
    int size;
    T* array = new T[size];
public:
    Stack()
    {
        std::cout << "Type size for new element\t";
        std::cin >> size;
        std::cout << std::endl;
    }
    void FillFromZero()
    {
        for (int i = 0; i < size; i++)
        {
            *(array + i) = i;
        }
    }
    void Show()
    {
        for (int i = 0; i < size; i++)
        {
            std::cout << *(array + i) << '\t';
        }
        std::cout << '\n';
    }
    void Push(T value)
    {
        size++;
        *(array + (size - 1)) = value;
    }
    void Pop()
    {
        *(array + (size - 1)) = NULL;
        size--;
    }
};


int main()
{
    Stack<int> data;
    data.FillFromZero();
    data.Show();
    data.Push(155);
    data.Show();
    data.Pop();
    data.Show();
}